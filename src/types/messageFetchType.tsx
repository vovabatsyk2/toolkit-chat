export type MessageFetchType = {
	id: string,
	userId: string,
	avatar: string,
	user: string,
	text: string,
	createdAt: string,
	editedAt: string,
	isLiked?: boolean
}
