export type UserFetchType = {
	id: number,
	username: string,
	email: string;
}
