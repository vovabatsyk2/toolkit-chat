import { configureStore } from '@reduxjs/toolkit';
import messagesReducer from  '../app/features/messagesSlice'
import userReducer from  '../app/features/usersSlice'

export const store = configureStore({
	reducer: {
		messages: messagesReducer,
		users: userReducer
	},
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
