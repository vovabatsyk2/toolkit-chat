import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserFetchType } from '../../types/userFetchType';

const initialState: UserFetchType[] = [{
	id: 0,
	username: '',
	email: ''
}];

export const usersSlice = createSlice({
	name: 'users',
	initialState,
	reducers: {
		addUser: (state, action: PayloadAction<UserFetchType>) => {
			const user:UserFetchType = {
				id: action.payload.id,
				username: action.payload.username,
				email: action.payload.email
			}
			state.push(user)
		},
		removeUser: (state, action: PayloadAction<number>) => {
			state.splice(action.payload, 1);
		},

		editUser:(state, action:PayloadAction<UserFetchType>) => {
			const user = state.find(x => x.id === action.payload.id)
			if (user) {
				user.username = action.payload.username
				user.email = action.payload.email
			}
		}
	},
});

export const {addUser, removeUser, editUser } = usersSlice.actions;

export default usersSlice.reducer;
