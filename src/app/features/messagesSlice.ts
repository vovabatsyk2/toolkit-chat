import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MessageFetchType } from '../../types/messageFetchType';

const initialState: MessageFetchType[] = [{
	id: '',
	userId: '',
	avatar: '',
	user: '',
	text: '',
	createdAt: '',
	editedAt: '',
	isLiked: false
}];

export const messagesSlice = createSlice({
	name: 'messages',
	initialState,
	reducers: {
		addMessage: (state, action: PayloadAction<MessageFetchType>) => {
			state.push(action.payload)
		},
		removeMessage: (state, action: PayloadAction<number>) => {
			state.splice(action.payload, 1);
		},
		toggleLike:(state, action:PayloadAction<string>) => {
			const message = state.find(x => x.id === action.payload)
			if (message) {
				message.isLiked = !message.isLiked
			}
		},
		editMessage:(state, action:PayloadAction<MessageFetchType>) => {
			const message = state.find(x => x.id === action.payload.id)
			if (message) {
				message.text = action.payload.text
				message.editedAt = action.payload.editedAt
			}
		}
	},
});

export const { addMessage, removeMessage, toggleLike, editMessage } = messagesSlice.actions;

export default messagesSlice.reducer;
