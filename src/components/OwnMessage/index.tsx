import { FC, useState } from 'react';
import { MessageFetchType } from '../../types/messageFetchType';
import { getFormattedDate } from '../../helpers/getFormatedDate';
import { useDispatch } from 'react-redux';
import { removeMessage } from '../../app/features/messagesSlice';

type OwnMessageProps = {
	message: MessageFetchType,
	index: number
}

export const OwnMessage: FC<OwnMessageProps> = ({ message, index }) => {
	const [value, setValue] = useState(message.text);
	const [isEdit, setIsEdit] = useState(false);
	const dispatch = useDispatch();

	const handleEditMessage = (e: any) => {
		e.preventDefault();
		setIsEdit(false);
	};

	return (
		<div className="ms-auto own-message d-flex flex-column p-2 m-2 mb-3">
			<div className="message-text">
				<textarea disabled={!isEdit}
									value={value}
									onChange={(e) => setValue(e.target.value)}/>
			</div>
			<div className="d-flex align-items-center actions">
				{
					!isEdit
						? <>
							<a className="btn" onClick={() => setIsEdit(true)}><i className="fa fa-edit"></i></a>
							<a className="btn" onClick={() => dispatch(removeMessage(index))}><i className="fa fa-trash"></i></a>
						</>

						: <a className="btn btn-success mt-2" onClick={handleEditMessage}>Save</a>
				}
			</div>
			<div className="message-time time ">
				{
					message.editedAt === ''
						? getFormattedDate('current-hh:mm')
						: `edited at ${message.editedAt}`
				}
			</div>
		</div>
	);
};
