import { FC } from 'react';
import { UserFetchType } from '../../types/userFetchType';
import { useDispatch } from 'react-redux';
import { removeUser } from '../../app/features/usersSlice';
import { useNavigate } from 'react-router-dom';

type UserProps = {
	user: UserFetchType,
	index: number
}

export const UserInfo: FC<UserProps> = ({ user, index }) => {
	const dispatch = useDispatch();
	const navigate = useNavigate();
	return (
		<div className="user-info  d-flex flex-row justify-content-between p-2 m-2">
			<div className="user-info__email p-2">{user.email}</div>
			<div className="user-info__action d-flex flex-row justify-content-between">
				<button className="btn btn-warning edit-user mx-2" onClick={() => navigate(`/users/${user.id}`)}><i
					className="fa fa-edit"></i></button>
				<button className="btn btn-danger delete-user mx-2" onClick={() => dispatch(removeUser(index))}><i
					className="fa fa-trash"></i></button>
			</div>
		</div>
	);
};
