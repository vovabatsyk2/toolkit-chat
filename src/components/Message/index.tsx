import { FC } from 'react';
import { MessageFetchType } from '../../types/messageFetchType';
import { getFormattedDate } from '../../helpers/getFormatedDate';
import { useDispatch } from 'react-redux';
import { toggleLike } from '../../app/features/messagesSlice';

type MessageProps = {
	message: MessageFetchType
}

export const Message: FC<MessageProps> = ({ message }) => {
	const dispatch = useDispatch();

	return (
		<div className="message d-flex justify-content-between align-items-center p-2 m-2">
			<div className="user-info-wrapper d-flex flex-column align-items-center">
				<img
					src={message.avatar}
					alt="user-avatar"
					className="message-user-avatar"
				/>
				<div className="message-user-name">{message.user}</div>
			</div>
			<div className="message-text-wrapper p-2">
				<div className="message-text">
					{message.text}
				</div>
				<div className="message-time">{getFormattedDate('hh:mm', message.createdAt)}</div>
			</div>
			<a className="message-like btn flex-grow-1" onClick={() => dispatch(toggleLike(message.id))}>
				<i className={`fa fa-thumbs-up ${message.isLiked ? 'like' : ''}  `}></i>
			</a>
		</div>
	);
};
