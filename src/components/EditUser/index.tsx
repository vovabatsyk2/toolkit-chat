import { FC, useEffect, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { UserFetchType } from '../../types/userFetchType';
import { editUser } from '../../app/features/usersSlice';

export const EditUser: FC = () => {
	const users = useSelector((state: RootState) => state.users);
	const [user, setUser] = useState<UserFetchType>({
		username: '',
		email: '',
		id: 0,
	});

	let { id } = useParams();
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const nameInputRef = useRef<HTMLInputElement>(null);
	const emailInputRef = useRef<HTMLInputElement>(null);

	useEffect(() => {
		const userFromData = users.find(u => u.id === Number(id));
		if (userFromData) {
			setUser(userFromData);
		}
	}, []);


	const handleSaveUser = () => {
		const name = nameInputRef.current?.value!;
		const email = emailInputRef.current?.value!;
		if (email.length && name.length) {
			dispatch(editUser({
				id: Number(id),
				username: nameInputRef.current?.value!,
				email: emailInputRef.current?.value!,
			}));
			navigate('/users');
		}
	};

	return (
		<div className="container mt-5">
			<h2 className="text-center">Edit User {id}</h2>
			<div className="form">
				<div className="mb-3">
					<label className="form-label">Name</label>
					<input type="text"
								 className="form-control"
								 ref={nameInputRef}
								 placeholder={user.username}
					/>
				</div>
				<div className="mb-3">
					<label className="form-label">Email</label>
					<input type="email"
								 className="form-control"
								 ref={emailInputRef}
								 placeholder={user.email}
					/>
				</div>
				<button className="btn btn-primary m-2" onClick={handleSaveUser}>Save</button>
				<button className="btn btn-secondary m-2" onClick={() => navigate('/users')}>Cancel</button>
			</div>
		</div>
	);
};
