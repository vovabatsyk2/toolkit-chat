import { FC } from 'react';
import Logo from '../../assets/images/chat-logo.svg';
import { useSelector } from 'react-redux';
import { RootState } from '../../app/store';
import { getFormattedDate } from '../../helpers/getFormatedDate';
import { logOut } from '../../services/adminService';
import { useNavigate } from 'react-router-dom';

export const Header: FC = () => {
	const messages = useSelector((state: RootState) => state.messages);
	const navigate = useNavigate();

	const logOutHandler = () => {
		logOut('user');
		navigate('/login');
	};

	return (
		<header className="header d-flex flex-row">
			<div className="d-flex header-title px-3">
				<img src={Logo} alt="chat-logo"/>
				<h2 className="p-2">Chat App</h2>
			</div>
			<div className="d-flex align-items-center ms-auto px-3">
				<div className="header-users-count p-2"><span> {messages.map(item => item.user)
					.filter(((value, index, array) =>
						array.indexOf(value) === index)).length} participants</span></div>
				<div className="header-messages-count p-2"><span> {messages.length} messages</span></div>
				<div className="header-last-message-date p-2">
					<small>{getFormattedDate('dd.mm.yyyy hh: mm', messages[messages.length - 1].createdAt)}</small>
				</div>
				<button className="btn btn-outline-danger m-2 logout" onClick={logOutHandler}>Log Out</button>
			</div>
		</header>
	);
};
