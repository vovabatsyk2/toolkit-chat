import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addMessage } from '../../app/features/messagesSlice';

export const MessageInput: FC = () => {
	const [message, setMessage] = useState('');
	const dispatch = useDispatch();

	const handleAddMessageText = () => {
		if (!message) return;
		dispatch(addMessage({
			id: '',
			userId: '',
			avatar: '',
			user: '',
			text: message,
			createdAt: new Date().toISOString(),
			editedAt: '',

		}));
		setMessage('');
	};

	return (
		<div className="message-input d-flex px-3 py-2">
			<input
				type="text"
				name="message"
				className="message-input-text form-control flex-grow-1"
				placeholder="Your message..."
				value={message}
				onChange={(event) => setMessage(event.target.value)}
			/>
			<button className="message-input-button btn btn-success" onClick={handleAddMessageText}>
				<i className="fa fa-paper-plane"></i><span className="input-button-text">&nbsp;Send</span>
			</button>
		</div>
	);
};
