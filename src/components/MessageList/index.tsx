import { FC } from 'react';
import { Message } from '../Message';
import { OwnMessage } from '../OwnMessage';
import { useSelector } from 'react-redux';
import { RootState } from '../../app/store';

type MessageListProps = {}

export const MessageList: FC<MessageListProps> = () => {
	const messages = useSelector((state: RootState) => state.messages);

	return (
		<main className="message-list d-flex flex-column">
			{
				messages.length && messages.map((message, i) => {
					if (message.text !== '') {
						return (message.user === ''
							? <OwnMessage message={message} index={i} key={`${i}-${Date.now()}`}/>
							: <Message message={message} key={`${i}-${message.id}`}/>);
					}
				}).reverse()
			}
		</main>
	);
};
