import { FC, useState } from 'react';
import { useDispatch } from 'react-redux';
import { addUser } from '../../app/features/usersSlice';

type ModalAddProps = {
	closeModal: () => void
}

export const ModalAdd: FC<ModalAddProps> = ({ closeModal }) => {
	const dispatch = useDispatch();
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');


	const handleAddUser = () => {
		if (name && password && email) {
			dispatch(addUser({
				id: Date.now(),
				username: name,
				email,
			}));
			setName('');
			setEmail('');
			setPassword('');

			closeModal();
		}
	};

	return (
		<>
			<div className="text-center">User Name</div>
			<label className="form-label">Username</label>
			<input type="text" className="form-control m-2" value={name} onChange={e => setName(e.target.value)}/>
			<label className="form-label">Email</label>
			<input type="email" className="form-control m-2" value={email} onChange={e => setEmail(e.target.value)}/>
			<label className="form-label">Password</label>
			<input type="password" className="form-control m-2" value={password} onChange={e => setPassword(e.target.value)}/>
			<button className="btn btn-primary m-2" onClick={handleAddUser}>Save</button>
			<button className="btn btn-secondary" onClick={closeModal}>Cancel</button>
		</>
	);
};
