export const saveUser = (name: string): void => {
	localStorage.setItem('user', name);
};

export const isUser = (): boolean => {
	const value = localStorage.getItem('user');
	if (value) return true;
	return false;
};
