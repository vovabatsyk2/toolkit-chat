export const saveAdmin = (name: string): void => {
	localStorage.setItem(name, 'admin');
};

export const isAdmin = ():boolean => {
	const value = localStorage.getItem('admin')
	if (value === 'admin') return true
	return false
}

export const logOut = (name:string) => {
	if (localStorage.getItem(name)) {
		localStorage.removeItem(name)
	}
}
