type FormatDate = 'dd.mm.yyyy hh: mm' | 'hh:mm' | 'current-hh:mm' | 'current-dd.mm.yyyy hh: mm'

export const getFormattedDate = (type: FormatDate, date?: string) => {
	let today = new Date();
	const dd = String(today.getDate()).padStart(2, '0');
	const MM = String(today.getMonth() + 1).padStart(2, '0');
	const yyyy = today.getFullYear();
	const mm = today.getMinutes();
	const hh = today.getUTCHours();
	const time = `${hh}:${mm}`;
	const full = `${dd}.${MM}.${yyyy}`;

	switch (type) {
		case 'current-dd.mm.yyyy hh: mm':
			return `${full} ${time}`;
		case 'current-hh:mm':
			return time;
		case 'dd.mm.yyyy hh: mm':
			if(date) {
				return formatDateWithHours(date)
			}
			break
		case 'hh:mm':
			if (date)
				return (date.split('T')[1]).slice(0, 5);
			break;
		default:
			return `${full} ${time}`;
	}
};

const formatDateWithHours = (date: string):string => {
	let format = date.split('T');
	format[0] = format[0].split('-').reverse().join('.');
	format[1] = format[1].slice(0, 5);
	return format.join(' ');
};

