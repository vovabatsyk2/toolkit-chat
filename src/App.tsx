import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Chat } from './pages/Chat';
import { Login } from './pages/Login';
import { Users } from './pages/Users';
import { EditUser } from './components/EditUser';

function App() {
	const urlMessages = 'https://edikdolynskyi.github.io/react_sources/messages.json'
	const  utlUsers = 'https://jsonplaceholder.typicode.com/users?_limit=5'
	return (
		<BrowserRouter>
			<Routes>
				<Route path="/" element={<Chat url={urlMessages}/>}></Route>
				<Route path="/login" element={<Login url={utlUsers}/>}></Route>
				<Route path="/users">
					<Route index element={<Users/>}/>
					<Route path=":id" element={<EditUser/>}/>
				</Route>
			</Routes>
		</BrowserRouter>
	);
}

export default App;
