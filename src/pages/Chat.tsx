import { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { Header } from '../components/Header';
import { MessageInput } from '../components/MessageInput';
import { MessageList } from '../components/MessageList';
import { MessageFetchType } from '../types/messageFetchType';
import { fetchData } from '../services/fetchData';
import { useDispatch, useSelector } from 'react-redux';
import { addMessage } from '../app/features/messagesSlice';
import { Preloader } from '../components/Preloader';
import { isUser } from '../services/userService';
import { RootState } from '../app/store';

type ChatProps = {
	url: string
}

export const Chat: FC<ChatProps> = ({url}) => {
	const messages = useSelector((state: RootState) => state.messages);
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [loading, setLoading] = useState(true);

	useEffect(() => {
		if (!isUser()) {
			navigate('/login');
		}
		try {
			fetchData(url).then((m: MessageFetchType[]) => {
				if (messages.length <= 1) {
					m.forEach(a => {
						dispatch(addMessage(a));
					});
				}
				setLoading(false);
			});
		} catch (e) {
			console.error(e);
		}

	}, []);

	return (
		<>
			{
				loading ? <Preloader/>
					: (
						<div className="chat container">
							<Header/>
							<MessageList/>
							<MessageInput/>
							<button onClick={() => navigate('/layout')}>to users</button>
						</div>
					)
			}

		</>
	);
};
