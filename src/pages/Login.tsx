import { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { fetchData } from '../services/fetchData';
import { UserFetchType } from '../types/userFetchType';
import { addUser } from '../app/features/usersSlice';
import { useDispatch, useSelector } from 'react-redux';
import { isAdmin, saveAdmin } from '../services/adminService';
import { RootState } from '../app/store';
import { isUser, saveUser } from '../services/userService';

type LoginProps = {
	url:string
}

export const Login: FC<LoginProps> = ({url}) => {
	const users = useSelector((state: RootState) => state.users);
	const [userName, setUserName] = useState('');
	const [userPassword, setUserPassword] = useState('');
	const navigate = useNavigate();
	const dispatch = useDispatch();


	useEffect(() => {
		try {
			fetchData(url)
				.then((data: UserFetchType[]) => {
					data.forEach((user) => {
						if(users.length <= 1) {
							dispatch(addUser(user));
						}
					});
					if (isAdmin()) navigate('/users');
					if (isUser()) navigate('/')
				});
		} catch (e) {
			console.error(e);
		}

	}, []);

	const handleSubmit = () => {
		if (userName === 'admin' && userPassword === 'admin') {
			saveAdmin(userName);
			navigate('/users');
		}
		const user = users.find((u: UserFetchType) => u.username === userName);

		if(user) {
			saveUser(user.username)
			navigate('/');
		};

	};

	return (
		<div className="container mt-5 col-md-4">
			<h1 className="text-center">Log In</h1>
			<div className="form">
				<div className="mb-3">
					<label className="form-label">Username</label>
					<input type="text"
								 className="form-control"
								 value={userName}
								 onChange={(e) => setUserName(e.target.value)}/>
				</div>
				<div className="mb-3">
					<label  className="form-label">Password</label>
					<input type="password"
								 className="form-control"
								 value={userPassword}
								 onChange={(e) => setUserPassword(e.target.value)}/>
				</div>
				<button className="btn btn-primary" onClick={handleSubmit}>Log in</button>
			</div>
		</div>
	);
};
