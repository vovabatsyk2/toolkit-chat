import { FC, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { UserInfo } from '../components/UserInfo';
import { UserFetchType } from '../types/userFetchType';
import { useSelector } from 'react-redux';
import { RootState } from '../app/store';
import Modal from 'react-modal';
import { isAdmin, logOut } from '../services/adminService';
import { ModalAdd } from '../components/ModalAdd';

Modal.setAppElement('#root');

export const Users: FC = () => {
	const users = useSelector((state: RootState) => state.users);
	const navigate = useNavigate();
	const [modalIsOpen, setIsOpen] = useState(false);

	useEffect(() => {
		if (!isAdmin()) {
			navigate('/login');
		}
	}, []);

	const addUserHandler = () => {
		setIsOpen(true);
	};

	const closeModal = () => {
		setIsOpen(false);
	};

	const logAutHandler = () => {
		logOut('admin');
		navigate('/login');
	};

	return (
		<>
			<div className="users container">
				<Modal
					isOpen={modalIsOpen}
					onRequestClose={closeModal}
					contentLabel="Example Modal"
				>
					<ModalAdd closeModal={closeModal}/>
				</Modal>
				<div className="add-user d-flex justify-content-between">
					<button className="btn btn-primary m-2" onClick={addUserHandler}>Add new User</button>
					<button className="btn btn-danger m-2" onClick={logAutHandler}>Log Out</button>
				</div>
				{
					users && users.map((user: UserFetchType, index) => {
						if (user.id) {
							return (<UserInfo user={user} key={user.id} index={index}/>);
						}
					})
				}
			</div>
		</>
	);
};
